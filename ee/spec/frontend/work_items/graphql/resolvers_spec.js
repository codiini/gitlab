import createMockApollo from 'helpers/mock_apollo_helper';
import { updateNewWorkItemCache } from '~/work_items/graphql/resolvers';
import workItemByIidQuery from '~/work_items/graphql/work_item_by_iid.query.graphql';
import updateNewWorkItemMutation from '~/work_items/graphql/update_new_work_item.mutation.graphql';
import {
  WIDGET_TYPE_COLOR,
  WIDGET_TYPE_ROLLEDUP_DATES,
  WIDGET_TYPE_HEALTH_STATUS,
  WIDGET_TYPE_ITERATION,
} from '~/work_items/constants';
import { createWorkItemQueryResponse } from 'jest/work_items/mock_data';

describe('work items graphql resolvers', () => {
  describe('updateNewWorkItemCache', () => {
    let mockApolloClient;

    const fullPath = 'fullPath';
    const fullPathWithId = 'fullPath-issue-id';
    const iid = 'new-work-item-iid';
    const mockLocalIteration = {
      __typename: 'Iteration',
      id: 'gid://gitlab/Iteration/46697',
      title: null,
      startDate: '2024-08-26',
      dueDate: '2024-09-01',
      webUrl: 'http://127.0.0.1:3000/groups/flightjs/-/iterations/46697',
      iterationCadence: {
        __typename: 'IterationCadence',
        id: 'gid://gitlab/Iterations::Cadence/5042',
        title:
          'Tenetur voluptatem necessitatibus velit natus et ut animi deleniti adipisci voluptas.',
      },
    };

    const mutate = (input) => {
      mockApolloClient.mutate({
        mutation: updateNewWorkItemMutation,
        variables: {
          input: {
            workItemType: 'issue',
            fullPath,
            ...input,
          },
        },
      });
    };

    const query = async (widgetName = null) => {
      const queryResult = await mockApolloClient.query({
        query: workItemByIidQuery,
        variables: { fullPath: fullPathWithId, iid },
      });

      if (widgetName == null) return queryResult.data.workspace.workItem;

      return queryResult.data.workspace.workItem.widgets.find(({ type }) => type === widgetName);
    };

    beforeEach(() => {
      const mockApollo = createMockApollo([], {
        Mutation: {
          updateNewWorkItem(_, { input }, { cache }) {
            updateNewWorkItemCache(input, cache);
          },
        },
      });
      mockApollo.clients.defaultClient.cache.writeQuery({
        query: workItemByIidQuery,
        variables: { fullPath: fullPathWithId, iid },
        data: createWorkItemQueryResponse.data,
      });
      mockApolloClient = mockApollo.clients.defaultClient;
    });

    describe('with healthStatus input', () => {
      it('updates health status', async () => {
        await mutate({ healthStatus: 'onTrack' });

        const queryResult = await query(WIDGET_TYPE_HEALTH_STATUS);
        expect(queryResult).toMatchObject({ healthStatus: 'onTrack' });
      });

      it('clears health status', async () => {
        await mutate({ healthStatus: null });

        const queryResult = await query(WIDGET_TYPE_HEALTH_STATUS);
        expect(queryResult).toMatchObject({ healthStatus: null });
      });
    });

    describe('with color input', () => {
      it('updates color', async () => {
        await mutate({ color: '#000' });

        const queryResult = await query(WIDGET_TYPE_COLOR);
        expect(queryResult).toMatchObject({ color: '#000' });
      });
    });

    describe('with rolledUpDates input', () => {
      it('updates rolledUpDates', async () => {
        await mutate({
          rolledUpDates: {
            dueDateIsFixed: true,
            startDateIsFixed: true,
            dueDateFixed: '2024-02-02',
            startDateFixed: '2023-12-22',
          },
        });

        const queryResult = await query(WIDGET_TYPE_ROLLEDUP_DATES);
        expect(queryResult).toMatchObject({
          dueDate: '2024-02-02',
          dueDateFixed: '2024-02-02',
          dueDateIsFixed: true,
          startDate: '2023-12-22',
          startDateFixed: '2023-12-22',
          startDateIsFixed: true,
        });
      });
    });

    describe('with iteration input', () => {
      it('updates iteration', async () => {
        await mutate({
          iteration: mockLocalIteration,
        });

        const queryResult = await query(WIDGET_TYPE_ITERATION);
        expect(queryResult).toMatchObject({
          iteration: mockLocalIteration,
        });
      });
    });
  });
});
