# frozen_string_literal: true

module Mutations
  module Issuables
    module CustomFields
      class Update < BaseMutation
        graphql_name 'CustomFieldUpdate'

        authorize :admin_custom_field

        field :custom_field, Types::Issuables::CustomFieldType,
          null: true,
          description: 'Updated custom field.'

        argument :id, ::Types::GlobalIDType[::Issuables::CustomField],
          required: true,
          description: copy_field_description(Types::Issuables::CustomFieldType, :id)

        argument :name, GraphQL::Types::String,
          required: true,
          description: copy_field_description(Types::Issuables::CustomFieldType, :name)

        def resolve(id:, **args)
          custom_field = authorized_find!(id: id)

          response = ::Issuables::CustomFields::UpdateService.new(
            custom_field: custom_field,
            current_user: current_user,
            params: args
          ).execute

          response_object = response.payload[:custom_field] if response.success?
          response_errors = response.error? ? Array(response.errors) : []

          {
            custom_field: response_object,
            errors: response_errors
          }
        end
      end
    end
  end
end
